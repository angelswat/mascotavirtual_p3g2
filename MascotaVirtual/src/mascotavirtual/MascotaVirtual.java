/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotavirtual;
import Escenas.UIPrincipal;
import Globales.Constantes;
import Juegos.UIJuego;
import java.io.IOException;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Clase principal, que pone en marcha el juego.
 * @author Angel
 */
public class MascotaVirtual extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        Scene scene = new Scene(new UIPrincipal().getRoot(),Constantes.ANCHOPANTALLA,Constantes.ALTURAPANTALLA);
        primaryStage.setTitle("Mascota Virtual");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Metodo que genera un escenario cargado de componentes.
     * @param primaryStage Escenario donde se crea el entorno.
     * @param pane Pane donde se agregan los componentes del entorno.
     * @throws IOException Lanza alerta de fallo de escena.
     */
    public void start(Stage primaryStage,BorderPane pane) throws IOException {
        Scene scene1 = new Scene(pane,Constantes.ANCHOPANTALLA,Constantes.ALTURAPANTALLA);
        primaryStage.setTitle("Mascota Virtual");
        primaryStage.setScene(scene1);
        primaryStage.show();
    }
    
    /**
     * Metodo que genera un escenario cargado de componentes.
     * @param primaryStage Escenario donde se crea un entorno.
     * @param juego Pane cargado de componentes.
     * @throws IOException Lanza excepcion por fallo de escena.
     */
    public void start(Stage primaryStage,UIJuego juego) throws IOException {
        Scene scene1 = new Scene(juego.getJuego(),Constantes.ANCHOPANTALLA,Constantes.ALTURAPANTALLA);
        scene1.onKeyPressedProperty().bind(juego.getJuego().onKeyPressedProperty());
        primaryStage.setTitle("Mascota Virtual");
        primaryStage.setScene(scene1);
        primaryStage.show();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
