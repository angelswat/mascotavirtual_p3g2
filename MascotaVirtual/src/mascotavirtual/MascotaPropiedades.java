/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotavirtual;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase que define las propiedades de la mascota virtual.
 * @author Ediso
 */
public class MascotaPropiedades implements Serializable {

    private String usuario;
    private String nombrePet;
    private int alimentacion = 10;
    private int animo = 10;
    private int dinero = 100;
    private int limpieza = 10;
    private String direccion;
    private SimpleDateFormat horaNac;
    private SimpleDateFormat fechaNac;
    private String hora;
    private String fecha;
    private int edad;
    private int edadMaxima = 500;
    private Date hoy = new Date();

    /**
     * Constructor de la clase, define las propiedades de la mascota.
     * @param usuario Nombre del usuario
     * @param nombrePet Nombre de la mascota
     * @param direccion Path de la imagen de la mascota
     * @param alimentacion Alimentacion de la mascota
     * @param animo Animo de la mascota
     * @param dinero Dinero de la mascota
     * @param limpieza Limpieza de la mascota
     * @param edad Edad de la mascota
     */
    public MascotaPropiedades(String usuario, String nombrePet, String direccion, int alimentacion, int animo, int dinero, int limpieza
            ,int edad) {
   
        this.usuario = usuario;
        this.alimentacion = alimentacion;
        this.animo = animo;
        this.dinero = dinero;
        this.limpieza = limpieza;
        this.nombrePet = nombrePet;
        this.direccion = direccion;
        this.edad=edad;
    }

    /**
     * Devuelve la alimentacion de la mascota.
     * @return Alimentacion
     */
    public int getAlimentacion() {
        return alimentacion;
    }

    /**
     * Setea la alimentacion de la mascota
     * @param alimentacion Alimentacion
     */
    public void setAlimentacion(int alimentacion) {
       this.alimentacion = alimentacion;
    }

    /**
     * Devuelve el animo
     * @return Animo
     */
    public int getAnimo() {
        return animo;
    }

    /**
     * Setea el animo
     * @param animo Animo
     */
    public void setAnimo(int animo) {
        this.animo = animo;
    }

    /**
     * Devuelve el dinero
     * @return Dinero
     */
    public int getDinero() {
        return dinero;
    }

    /**
     * Setea el dinero
     * @param dinero Dinero
     */
    public  void setDinero(int dinero) {
        this.dinero = dinero;
    }

    /**
     * Devuelve la limpieza
     * @return Limpieza
     */
    public int getLimpieza() {
        return limpieza;
    }

    /**
     * Setea la limpieza
     * @param limpieza Limpieza
     */
    public void setLimpieza(int limpieza) {
        this.limpieza = limpieza;
    }

    /**
     * Devuelve un path de una imagen
     * @return Path de la imagen
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Setea el path de la imagen
     * @param direccion Path de imagen
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Devuelve la hora
     * @return Hora
     */
    public String getHora() {
        return hora;
    }

    /**
     * Setea la hora
     * @param hora Hora
     */
    public void setHora(String hora) {
        this.hora = hora;
    }

    /**
     * Devuelve la fecha
     * @return Fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Setea la fecha
     * @param fecha Fecha
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * Devuelve el nombre del usuario
     * @return Nombre del usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Setea el nombre del usuario
     * @param usuario Nombre del usuario
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * Devuelve el nombre de la mascota
     * @return Nombre de la mascota
     */
    public String getNombrePet() {
        return nombrePet;
    }

    /**
     * Setea el nombre de la mascota
     * @param nombrePet Nombre de la mascota
     */
    public void setNombrePet(String nombrePet) {
        this.nombrePet = nombrePet;
    }

    /**
     * Devuelve la edad de la mascota
     * @return Edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * Setea la edad de la mascota
     * @param edad Edad
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }
    

}
