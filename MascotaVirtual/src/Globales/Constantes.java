/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Globales;

/**
 * Clase que almacena constantes para todo el juego.
 * @author Angel Antonio Encalada Davila
 */
public class Constantes {

    /**
     * Altura de la pantalla.
     */
    public final static double ALTURAPANTALLA = 500;

    /**
     * Ancho de la pantalla.
     */
    public final static double ANCHOPANTALLA=700;
    
    /**
     * Precio del atun.
     */
    public static final int ATUN_PRECIO=2;

    /**
     * Puntaje del atun.
     */
    public static final int ATUN_PUNTOS=2;

    /**
     * Precio de la gaseosa.
     */
    public static final int GASEOSA_PRECIO=1;

    /**
     * Puntaje de la gaseosa.
     */
    public static final int  GASEOSA_PUNTOS=1;

    /**
     * Precio de la lasagna.
     */
    public static final int LASAGNA_PRECIO=5;

    /**
     * Puntaje de la lasagna.
     */
    public static final int LASAGNA_PUNTOS=4;

    /**
     * Precio de la pizza.
     */
    public static final int PIZZA_PRECIO=2;

    /**
     * Puntaje de la pizza.
     */
    public static final int PIZZA_PUNTOS=2;

    /**
     * Precio del pollo.
     */
    public static final int POLLO_PRECIO=5;

    /**
     * Puntaje del pollo.
     */
    public static final int POLLO_PUNTOS=4;
}
