/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Juegos;

import Componentes.ManejoArchivos;
import Escenas.UILivingRoom;
import Globales.Constantes;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

/**
 * Clase que controla la ejecucion del juego de la mascota.
 * @author Angel Antonio Encalada Davila
 */
public class UIJuego {

    private Pane juego;
    private ImageView mascota;
    private Arco arco;
    private Flecha f;

    private Label lblDinero;
    private Label lblTextDinero;
    private HBox money;

    private ArrayList<Cofre> cofres;
    private ArrayList<Cofre> bombas;

    private Timeline dinero;

    /**
     * Constructor de la clase, inicializa los controles del juego.
     */
    public UIJuego() {
        inicializarControles();
        ubicarElementos();
        Thread t = new Thread(new EjecutorAnimacion());
        t.start();
    }

    /**
     * Metodo que inicia los controles del juego.
     */
    public void inicializarControles() {
        juego = new Pane();
        cofres = new ArrayList<Cofre>();
        bombas = new ArrayList<Cofre>();
        Image image = new Image(getClass().getResource("/Imagen/UIJuego/fondo.png").toExternalForm());
        BackgroundImage fondo = new BackgroundImage(image, null, null, null, new BackgroundSize(Constantes.ANCHOPANTALLA, Constantes.ALTURAPANTALLA, false, false, false, false));
        juego.setBackground(new Background(fondo));
        manejarEvento();
        manejarTiempo();
    }

    /**
     * Metodo que maneja los eventos que ocurren dentro del juego.
     */
    public void manejarEvento() {
        juego.setOnKeyPressed(e -> moverObjeto(e));
        juego.setOnMouseClicked(e -> lanzarFlecha(e));
    }

    /**
     * Metodo que lanza un flecha cuando se da clic.
     * @param e Evento
     */
    public void lanzarFlecha(MouseEvent e) {
        Flecha flecha = new Flecha();
        this.f = flecha;
        juego.getChildren().add(flecha.getImage());
        flecha.animacion(mascota.getLayoutX(), e.getX(), e.getY());

        for (Cofre c : cofres) {
            if (Colision(c.getImage(), f.getImage())) {
                c.getPane().setOnMouseClicked(l -> {
                    removerDePanel(l);
                    aumento(Integer.parseInt(c.getText().getText().toString()));
                });

            }
        }
        for (Cofre c2 : bombas) {
            if (Colision(c2.getImage(), f.getImage())) {
                c2.getPane().setOnMouseClicked(w -> {
                    System.out.println("BOMBAAA !!!");
                    UILivingRoom.juegoStage.close();
                });
            }
        }
    }

    /**
     * Metodo que mueve la mascota de un lado a otro segun el control.
     * @param e Evento
     */
    public void moverObjeto(KeyEvent e) {
        if (e.getCode().equals(KeyCode.RIGHT)) {
            mascota.setLayoutX(mascota.getLayoutX() + 5);
            Arco.image.setLayoutX(arco.image.getLayoutX() + 5);
        } else if (e.getCode().equals(KeyCode.LEFT)) {
            mascota.setLayoutX(mascota.getLayoutX() - 5);
            Arco.image.setLayoutX(arco.image.getLayoutX() - 5);
        }
    }

    /**
     * Metodo para ubicar los componentes dentro del pane.
     */
    public void ubicarElementos() {
        arco = new Arco();
        money = new HBox();

        ImageView imMoney = new ImageView(new Image("/Imagen/comida/money.png"));
        imMoney.setFitHeight(50);
        imMoney.setFitWidth(70);

        lblDinero = new Label();
        lblDinero.setGraphic(imMoney);
        lblTextDinero = new Label(ManejoArchivos.getDineroDes() + " $$");
        lblTextDinero.setFont(Font.font(15));
        lblTextDinero.setTextFill(Color.BLACK);
        lblTextDinero.setTextAlignment(TextAlignment.LEFT);
        money = new HBox();
        money.getChildren().addAll(lblDinero, lblTextDinero);
        money.setSpacing(20);
        money.setAlignment(Pos.CENTER_LEFT);

        String ARCHIVO_IMAGEN = ManejoArchivos.getDireccionDes();
        mascota = new ImageView(new Image(ARCHIVO_IMAGEN));
        mascota.setFitHeight(150);
        mascota.setFitWidth(70);
        mascota.setLayoutX(Constantes.ANCHOPANTALLA / 2);
        mascota.setLayoutY(Constantes.ALTURAPANTALLA - 200);
        arco.image.setLayoutX(mascota.getLayoutX() + 35);
        arco.image.setLayoutY(mascota.getLayoutY() + 50);
        money.setLayoutX(Constantes.ANCHOPANTALLA - 140);
        money.setLayoutY(0);
        juego.getChildren().addAll(mascota, arco.image, money);
    }

    /**
     * Metodo para crear un nuevo cofre.
     */
    public void crearNuevoCofre() {
        int random = ThreadLocalRandom.current().nextInt(-100, 100);
        Cofre c = new Cofre(Integer.toString(random));
        juego.getChildren().add(c.getPane());
        cofres.add(c);
    }

    /**
     * Metodo para crear bombas.
     */
    public void crearNuevaBomba() {
        Cofre c2 = new Cofre("S");
        juego.getChildren().add(c2.getPane());
        bombas.add(c2);
    }

    /**
     *  Metodo que valida una colision.
     * @param a Nodo
     * @param b Nodo
     * @return True si colisionan, false si no lo hacen.
     */
    public boolean Colision(Node a, Node b) {
        if (a.getBoundsInParent().intersects(b.getBoundsInParent())) {
            return true;
        }
        return false;
    }

    /**
     * Metodo que remueve objetos del pane.
     * @param e Evento 
     */
    public void removerDePanel(MouseEvent e) {
        juego.getChildren().remove(e.getSource());
    }

    /**
     * Metodo para manejar el tiempo durante el juego.
     */
    public void manejarTiempo() {
        dinero = new Timeline();
        KeyFrame kf2 = new KeyFrame(Duration.millis(1), e -> controlDinero());

        dinero.getKeyFrames().addAll(kf2);
        dinero.setCycleCount(Timeline.INDEFINITE);

        dinero.play();
    }

    /**
     * Metodo que controla la perdida o ganancia de dinero durante el juego.
     */
    public void controlDinero() {
        if (ManejoArchivos.getDineroDes() >= 0) {
            lblTextDinero.setText(Integer.toString(ManejoArchivos.getDineroDes()) + " $$");
            UILivingRoom.lblTextDinero.setText(Integer.toString(ManejoArchivos.getDineroDes()) + " $$");
        } else {
            lblTextDinero.setText("0" + " $$");
            UILivingRoom.lblTextDinero.setText("0" + " $$");
        }
    }

    /**
     * Metodo para sumar o restar a la cuenta el dinero ganado o perdido.
     * @param x Dinero
     */
    public void aumento(int x) {
        System.out.println("A tu cuenta se añaden " + x + " dolares");
        if (ManejoArchivos.getDineroDes() >= 0) {
            ManejoArchivos.setDineroDes(ManejoArchivos.getDineroDes() + x);
        } else {
            ManejoArchivos.setDineroDes(0);
        }
        System.out.println("Tu saldo actual es " + ManejoArchivos.getDineroDes() + " dolares");
    }

    /**
     * Clase interna referente a la thread del juego.
     */
    public class EjecutorAnimacion implements Runnable {

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                Platform.runLater(() -> crearNuevoCofre());
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    System.out.println(ex);
                }
            }
            for (int j = 0; j < 5; j++) {
                Platform.runLater(() -> crearNuevaBomba());
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    System.out.println(ex);
                }
            }
        }

    }

    /**
     * Devuelve el pane del juego
     * @return Pane del juego
     */
    public Pane getJuego() {
        return juego;
    }
}
