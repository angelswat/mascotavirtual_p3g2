/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Juegos;

import Globales.Constantes;
import javafx.animation.PathTransition;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Line;
import javafx.util.Duration;

/**
 * Clase que define las propiedades y metodos de las flechas del juego.
 * @author Angel Antonio Encalada Davila
 */
public class Flecha {
    private Line line;
    private static final String ARCHIVO_IMAGEN="/Imagen/UIJuego/arrow.png";
    private ImageView image;
    
    /**
     * Constructor de la clase, inicia los controles.
     */
    public Flecha(){
        inicializarElementos();
    }
    
    /**
     * Metodo para ubicar elementos y componentes en el pane.
     */
    public void inicializarElementos(){
        line=new Line();
        image= new ImageView(new Image(ARCHIVO_IMAGEN));
        image.setFitHeight(40);
        image.setFitWidth(40);
    }
    
    PathTransition animacion(double x,double y,double z){
        PathTransition transition= new PathTransition();
        transition.setNode(image);
        transition.setPath(parametrizarLinea(x,y,z));
        transition.setDuration(Duration.seconds(0.3));
        transition.play();
        return transition;
    }
    
    /**
     * Metodo que define la trayectoria de la flecha.
     * @param x Inicio en X
     * @param y Final en X
     * @param z Final en Y
     * @return Linea de trayectoria
     */
    public Line parametrizarLinea(double x,double y,double z){
        line.setStartX(x+5);
        line.setEndX(y);
        line.setEndY(z);
        line.setStartY(Constantes.ALTURAPANTALLA-100);
        return line;
    }
    
    /**
     * Devuelve la imagen de la flecha
     * @return Imagen de flecha
     */
    public ImageView getImage(){
        return image;
    }
}
