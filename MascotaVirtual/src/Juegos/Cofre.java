/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Juegos;

import Globales.Constantes;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * Clase que contiene las propiedades de los cofres tesorados del juego.
 * @author Angel Antonio Encalada Davila
 */
public class Cofre {
    private static final String ARCHIVO_IMAGEN="Imagen/UIJuego/cofre.png";
    private Text text= new Text();
    private StackPane pane= new StackPane();
    private ImageView image;
    
    /**
     * Constructor de la clase, define algunos componentes internos.
     * @param texto Dinero 
     */
    public Cofre(String texto){
        image=new ImageView(new Image(ARCHIVO_IMAGEN));
        image.setFitHeight(60);
        image.setFitWidth(60);
        text.setFill(Color.WHITE);
        text.setFont(Font.font(15));
        text.setText(texto);
        agregarPane();
        animacion();
    }
    
    /**
     * Metodo que anima el cofre mientras aparecen en el juego.
     */
    public void animacion(){
        PathTransition transition= new PathTransition();
        transition.setNode(pane);
        Random r= new Random();
        int num=r.nextInt((int)Constantes.ANCHOPANTALLA);
        Line line = new Line();
        line.setStartX(num);
        line.setStartY(0);
        line.setEndX(num);
        line.setEndY(Constantes.ALTURAPANTALLA);
        int random = ThreadLocalRandom.current().nextInt(5,10);
        transition.setPath(line);
        transition.setDuration(Duration.seconds(random));
        transition.setCycleCount(Timeline.INDEFINITE);
        transition.autoReverseProperty();
        transition.play();
    }
    
    /**
     * Metodo para agregar componentes al pane.
     */
    public void agregarPane(){
        pane.getChildren().addAll(image,text);
    }
    
    /**
     * Devuelve el pane del juego
     * @return Pane del juego
     */
    public StackPane getPane(){
        return pane;
    }
    
    /**
     * Devuelve la imagen del cofre
     * @return Imagen del cofre
     */
    public ImageView getImage(){
        return image;
    }
    
    /**
     * Devuelve el texto inscrito en el cofre
     * @return Texto del cofre
     */
    public Text getText(){
        return text;
    }
}
