/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Juegos;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Clase que define las propiedades y acciones del arco de flecha.
 * @author Angel Antonio Encalada Davila
 */
public class Arco {
    
    private static final String ARCHIVO_IMAGEN= "/Imagen/UIJuego/arco.png";

    /**
     * Imagen del arco.
     */
    public static ImageView image;
    
    /**
     * Constructor de la clase, que inicializa los controles.
     */
    public Arco(){
        inicializarElementos();
    }
    
    /**
     * Metodo que inicia los controles del arco dentro del juego.
     */
    public void inicializarElementos(){
        image= new ImageView(new Image(ARCHIVO_IMAGEN));
        image.setFitHeight(100);
        image.setFitWidth(50);
    }
    
    /**
     * Devuelve la imagen del arco
     * @return Imagen arco
     */
    public ImageView image(){
        return image;
    }
    
}
