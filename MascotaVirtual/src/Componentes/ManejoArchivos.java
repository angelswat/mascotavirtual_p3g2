/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Componentes;

import Escenas.Alerta;
import Escenas.UIPrincipal;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import mascotavirtual.MascotaPropiedades;

/**
 * Clase que se encarga de la administracion de los archivos de registro de los
 * estados de la mascota.
 *
 * @author Ediso
 */
public class ManejoArchivos {

    private static int alimentacionDes;
    private static int animoDes;
    private static int dineroDes;
    private static int limpiezaDes;
    private static String direccionDes;
    private static String nombreUsuarioDes;
    private static String nombreMascotaDes;
    private static int vidaDes;
    private static int fechaDes;
    private static int fechaMin;

    /**
     * Metodo que genera un archivo con los parametros iniciales del juego, al
     * crear una mascota.
     *
     * @throws IOException Lanza excepcion por inexistencia de archivo.
     */
    public static void serializacion() throws IOException {
        MascotaPropiedades mascotaPropiedad = new MascotaPropiedades(nombreUsuarioDes, nombreMascotaDes,
                getDireccionDes(), 10, 10, 100, 10, 1);
        try (FileWriter file = new FileWriter(nombreUsuarioDes + ".json")) {
            System.out.println("Ingreso a serializar");
            Gson gson = new GsonBuilder().create();
            gson.toJson(mascotaPropiedad, file);
        }
    }

    /**
     * Metodo que guarda todos los estados y parametros de la mascota.
     *
     * @throws IOException Lanza excepcion por inexistencia de archivo.
     */
    public static void serializacionFinal() throws IOException {
        MascotaPropiedades mascotaPropiedad = new MascotaPropiedades(nombreUsuarioDes, nombreMascotaDes,
                getDireccionDes(), getAlimentacionDes(), getAnimoDes(), getDineroDes(), getLimpiezaDes(), getVidaDes());
        try (FileWriter file = new FileWriter(nombreUsuarioDes + ".json")) {
            System.out.println("Ingreso a serializar");
            Gson gson = new GsonBuilder().create();
            gson.toJson(mascotaPropiedad, file);
        }
    }

    /**
     * Metodo que elimina el archivo de registro de la mascota cuando muere.
     *
     * @param nombre Nombre de archivo.
     * @throws FileNotFoundException Lanza alerta por archivo no encontrado.
     */
    public static void borrarJson(String nombre) throws FileNotFoundException {
        File file = new File(nombre);
        file.deleteOnExit();
    }

    /**
     * Metodo que decodifica todos los parametros registrados sobre la mascota.
     * @param nombre Nombre de archivo.
     */
    public static void deserializacion(String nombre) {
        System.out.println("Ingreso a deserealizar");
        JsonParser jsonParser = new JsonParser();
        try {
            Object ob = jsonParser.parse(new FileReader(nombre + ".json"));
            JsonObject jsonOb = (JsonObject) ob;
            nombreUsuarioDes = jsonOb.get("usuario").getAsString();
            nombreMascotaDes = jsonOb.get("nombrePet").getAsString();
            alimentacionDes = jsonOb.get("alimentacion").getAsInt();
            animoDes = jsonOb.get("animo").getAsInt();
            dineroDes = jsonOb.get("dinero").getAsInt();
            limpiezaDes = jsonOb.get("limpieza").getAsInt();
            direccionDes = jsonOb.get("direccion").getAsString();
            vidaDes = jsonOb.get("edad").getAsInt();

            obtenerHoras(nombre);

            System.out.println("Usuario" + nombreUsuarioDes + "Mascota" + nombreMascotaDes + "Alimento" + alimentacionDes + "Animo" + animoDes + "Dinero" + dineroDes + "Limpieza:" + limpiezaDes + "Edad" + vidaDes);
        } catch (IOException e) {
            System.out.println("Deserealizar IOException");
            Alerta.lanzarAlerta();
            System.out.println(e);
            UIPrincipal.entradaStage.close();
        }
    }

    /**
     * Devuelve la alimentacion de la mascota
     * @return Alimentacion
     */
    public static int getAlimentacionDes() {
        return alimentacionDes;
    }

    /**
     * Setea la alimentacion
     * @param alimentacionDes Alimentacion
     */
    public static void setAlimentacionDes(int alimentacionDes) {
        ManejoArchivos.alimentacionDes = alimentacionDes;
    }

    /**
     * Devuelve el animo de la mascota
     * @return Animo de la mascota
     */
    public static int getAnimoDes() {
        return animoDes;
    }

    /**
     * Setea el animo de la mascota
     * @param animoDes Animo
     */
    public static void setAnimoDes(int animoDes) {
        ManejoArchivos.animoDes = animoDes;
    }

    /**
     * Devuelve el dinero de la mascota
     * @return Dinero de la mascota
     */
    public static int getDineroDes() {
        return dineroDes;
    }

    /**
     * Setea el dinero de la mascota
     * @param dineroDes Dinero
     */
    public static void setDineroDes(int dineroDes) {
        ManejoArchivos.dineroDes = dineroDes;
    }

    /**
     * Devuelve la limpieza de la mascota
     * @return Limpieza de la mascota
     */
    public static int getLimpiezaDes() {
        return limpiezaDes;
    }

    /**
     * Setea la limpieza de la mascota
     * @param limpiezaDes Limpieza
     */
    public static void setLimpiezaDes(int limpiezaDes) {
        ManejoArchivos.limpiezaDes = limpiezaDes;
    }

    /**
     * Devuelve el path de la imagen
     * @return Path de la imagen de la mascota
     */
    public static String getDireccionDes() {
        return direccionDes;
    }

    /**
     * Setea el path de la imagen
     * @param direccionDes Path de la imagen
     */
    public static void setDireccionDes(String direccionDes) {
        ManejoArchivos.direccionDes = direccionDes;
    }

    /**
     * Devuelve el nombre de usuario
     * @return Nombre del usuario o duenio de la mascota
     */
    public static String getNombreUsuarioDes() {
        return nombreUsuarioDes;
    }

    /**
     * Setea el nombre de usuario
     * @param nombreUsuarioDes
     */
    public static void setNombreUsuarioDes(String nombreUsuarioDes) {
        ManejoArchivos.nombreUsuarioDes = nombreUsuarioDes;
    }

    /**
     * Devuelve el nombre de la mascota
     * @return Nombre de la mascota
     */
    public static String getNombreMascotaDes() {
        return nombreMascotaDes;
    }

    /**
     * Setea el nombre de la mascota
     * @param nombreMascotaDes Nombre de la mascota
     */
    public static void setNombreMascotaDes(String nombreMascotaDes) {
        ManejoArchivos.nombreMascotaDes = nombreMascotaDes;
    }

    /**
     * Devuelve la vida de la mascota
     * @return Vida de la mascota
     */
    public static int getVidaDes() {
        return vidaDes;
    }

    /**
     * Setea la vida de la mascota
     * @param vidaDes Vida de la mascota
     */
    public static void setVidaDes(int vidaDes) {
        ManejoArchivos.vidaDes = vidaDes;
    }

    /**
     * Metodo que actualiza los estados, del juego en segundo plano.
     * @param nombreUsuario Nombre de usuario 
     */
    public static void obtenerHoras(String nombreUsuario) {
        String path = (nombreUsuario + ".json");
        File file = new File(path);
        long ms = file.lastModified();
        Date date = new Date();
        long tiempoFinal = date.getTime();
        fechaDes = (int) ((tiempoFinal - ms) / (3600000));
        fechaMin = (int) ((tiempoFinal - ms) / 600000);
        System.out.println("Resta" + fechaDes);
        setVidaDes(getVidaDes() + fechaDes);

        if ((getAlimentacionDes() - fechaMin) < 0) {
            setAlimentacionDes(0);
        } else {
            setAlimentacionDes(getAlimentacionDes() - fechaMin);
        }

        if ((getLimpiezaDes() - fechaMin) < 0) {
            setLimpiezaDes(0);
        } else {
            setLimpiezaDes(getLimpiezaDes() - fechaMin);
        }

        if ((getAnimoDes() - fechaMin) < 0) {
            setAnimoDes(0);
        } else {
            setAnimoDes(getAnimoDes() - fechaMin);
        }
    }

}
