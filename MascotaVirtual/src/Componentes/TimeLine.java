/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Componentes;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.ProgressBar;

/**
 * Clase que maneja las threads que controlan ciertos metodos del juego.
 *
 * @author Ediso
 */
public class TimeLine extends Thread {

    ProgressBar ps;

    /**
     * Constructor de las clase.
     */
    public TimeLine() {
        ps = new ProgressBar();
    }

    /**
     * Metodo run, donde se define las acciones que corren con la thread.
     */
    public void run() {
        for (int i = 0; i <= 1; i++) {
            try {
                restarPropiedades();
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(TimeLine.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Metodo que disminuye la alimentacion de la mascota cada minuto.
     */
    public void restarPropiedades() {
        ManejoArchivos.setAlimentacionDes(ManejoArchivos.getAlimentacionDes() - 1);
        System.out.println(ManejoArchivos.getAlimentacionDes());
    }

    /**
     * Devuelve la barra de progreso.
     * @return Barra de progreso
     */
    public ProgressBar getPs() {
        return ps;
    }
}
