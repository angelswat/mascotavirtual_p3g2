/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Escenas;

import Componentes.ManejoArchivos;
import Globales.Constantes;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 *Clase que se encarga de cargar el escenario para el comedor del juego
 * @author Angel Antonio Encalada Davila
 */
public class Comedor {

    private HBox superior;
    private HBox comidas;
    private VBox centro;
    private HBox dinero;
    private HBox indicador;

    private Button btnAtun;
    private Button btnGaseosa;
    private Button btnLasagna;
    private Button btnPizza;
    private Button btnPollo;
    private Label lblTitulo;
    private String direccion;
    private Label lblDinero;
    private Label textoDinero;

    private Label lblAlimentacion;

    /**
     *Se actualiza la barra de Alimentacion
     */
    public static ProgressBar pbAlimentacion = new ProgressBar((float) ManejoArchivos.getAlimentacionDes() / 10);

    /**
     *Se actualiza el indicador de Alimentacion
     */
    public static ProgressIndicator piAlimentacion = new ProgressIndicator((float) ManejoArchivos.getAlimentacionDes() / 10);

    private BorderPane root;

    /**
     *Constructor de la clase Comedor
     * @param direccion atributo que guarda la ruta de la mascota del usuario
     */
    public Comedor(String direccion) {
        this.direccion = direccion;
        iniciarControles();
    }

    /**
     *Metodo para cargar todos los elementos de la clase comedor
     */
    public void iniciarControles() {
        ImageView imMascota = new ImageView(new Image(direccion));
        imMascota.setFitHeight(250);
        imMascota.setFitWidth(200);

        superior = new HBox();
        comidas = new HBox();
        centro = new VBox();
        dinero = new HBox();
        indicador = new HBox();

        btnAtun = new Button();
        btnGaseosa = new Button();
        btnLasagna = new Button();
        btnPizza = new Button();
        btnPollo = new Button();
        lblTitulo = new Label();
        textoDinero = new Label();

        root = new BorderPane();

        textoDinero.setTextFill(Color.WHITE);
        textoDinero.setFont(Font.font(15));
        textoDinero.setAlignment(Pos.CENTER_LEFT);
        textoDinero.setText(String.valueOf(ManejoArchivos.getDineroDes()) + " $$");

        Image image1 = new Image(getClass().getResource("/Imagen/comida/atun.png").toExternalForm());
        ImageView imAtun = new ImageView();
        imAtun.setFitHeight(100);
        imAtun.setFitWidth(100);
        imAtun.setImage(image1);

        Image image2 = new Image(getClass().getResource("/Imagen/comida/gaseosa.png").toExternalForm());
        ImageView imGaseosa = new ImageView();
        imGaseosa.setFitHeight(100);
        imGaseosa.setFitWidth(50);
        imGaseosa.setImage(image2);

        Image image3 = new Image(getClass().getResource("/Imagen/comida/lasagna.png").toExternalForm());
        ImageView imLasagna = new ImageView();
        imLasagna.setFitHeight(100);
        imLasagna.setFitWidth(100);
        imLasagna.setImage(image3);

        Image image4 = new Image(getClass().getResource("/Imagen/comida/pizza.png").toExternalForm());
        ImageView imPizza = new ImageView();
        imPizza.setFitHeight(100);
        imPizza.setFitWidth(100);
        imPizza.setImage(image4);

        Image image5 = new Image(getClass().getResource("/Imagen/comida/pollo.png").toExternalForm());
        ImageView imPollo = new ImageView();
        imPollo.setFitHeight(100);
        imPollo.setFitWidth(150);
        imPollo.setImage(image5);

        Image image6 = new Image(getClass().getResource("/Imagen/comida/titulo.png").toExternalForm());
        ImageView imTitulo = new ImageView();
        imTitulo.setFitHeight(75);
        imTitulo.setFitWidth(300);
        imTitulo.setImage(image6);

        ImageView imMoney = new ImageView(new Image("/Imagen/comida/money.png"));
        imMoney.setFitHeight(50);
        imMoney.setFitWidth(70);
        lblDinero = new Label();
        lblDinero.setGraphic(imMoney);

        Image image7 = new Image(getClass().getResource("/Imagen/comida/fondo.png").toExternalForm());
        BackgroundImage fondo = new BackgroundImage(image7, null, null, null, new BackgroundSize(Constantes.ANCHOPANTALLA, Constantes.ALTURAPANTALLA, false, false, false, false));

        btnAtun.setGraphic(imAtun);
        btnGaseosa.setGraphic(imGaseosa);
        btnLasagna.setGraphic(imLasagna);
        btnPizza.setGraphic(imPizza);
        btnPollo.setGraphic(imPollo);
        lblTitulo.setGraphic(imTitulo);

        ImageView imTextAlimentacion = new ImageView(new Image("/Imagen/UIhabitacion/Feeding.png"));
        imTextAlimentacion.setFitHeight(40);
        imTextAlimentacion.setFitWidth(80);
        lblAlimentacion = new Label();
        lblAlimentacion.setGraphic(imTextAlimentacion);
        indicador.getChildren().addAll(imTextAlimentacion, pbAlimentacion, piAlimentacion);
        indicador.setSpacing(5);
        indicador.setAlignment(Pos.CENTER_LEFT);

        dinero.getChildren().addAll(lblDinero, textoDinero);
        dinero.setSpacing(10);
        dinero.setAlignment(Pos.CENTER);
        superior.getChildren().addAll(indicador, lblTitulo, dinero);
        superior.setAlignment(Pos.CENTER);
        superior.setSpacing(20);
        comidas.getChildren().addAll(btnAtun, btnGaseosa, btnLasagna, btnPizza, btnPollo);
        comidas.setAlignment(Pos.BOTTOM_CENTER);
        comidas.setSpacing(10);

        root.setTop(superior);
        centro.getChildren().addAll(imMascota, comidas);
        centro.setAlignment(Pos.CENTER);
        centro.setSpacing(30);
        root.setCenter(centro);
        root.setBackground(new Background(fondo));

        inicializarBotones();
    }

    /**
     *Metodo que se encarga de setear las acciones a los botones
     */
    public void inicializarBotones() {
        btnAtun.setOnAction(e -> confiBtnAtun());
        btnGaseosa.setOnAction(e -> confiBtnGaseosa());
        btnLasagna.setOnAction(e -> confiBtnLasagna());
        btnPizza.setOnAction(e -> confiBtnPizza());
        btnPollo.setOnAction(e -> confiBtnPollo());
    }

    /**
     *Metodo para restar dinero y aumentar la alimentacion acorde al alimento "Atun"
     */
    public void confiBtnAtun() {
        if (ManejoArchivos.getDineroDes() > 0) {
            ManejoArchivos.setDineroDes(ManejoArchivos.getDineroDes() - Constantes.ATUN_PRECIO);
            ManejoArchivos.setAlimentacionDes(ManejoArchivos.getAlimentacionDes() + Constantes.ATUN_PUNTOS);
            System.out.println("dinero" + ManejoArchivos.getDineroDes());
            textoDinero.setText(String.valueOf(ManejoArchivos.getDineroDes()) + " $$");
            UILivingRoom.pbAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);
            UILivingRoom.piAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);
        }
    }

    /**
     *Metodo para restar dinero y aumentar la alimentacion acorde al alimento "Gaseosa"
     */
    public void confiBtnGaseosa() {
        if (ManejoArchivos.getDineroDes() > 0) {
            ManejoArchivos.setDineroDes(ManejoArchivos.getDineroDes() - Constantes.GASEOSA_PRECIO);
            ManejoArchivos.setAlimentacionDes(ManejoArchivos.getAlimentacionDes() + Constantes.GASEOSA_PUNTOS);
            System.out.println("dinero" + ManejoArchivos.getDineroDes());
            textoDinero.setText(String.valueOf(ManejoArchivos.getDineroDes()) + " $$");
            UILivingRoom.pbAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);
            UILivingRoom.piAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);
        }
    }

    /**
     *Metodo para restar dinero y aumentar la alimentacion acorde al alimento "Lasagna"
     */
    public void confiBtnLasagna() {
        if (ManejoArchivos.getDineroDes() > 0) {
            ManejoArchivos.setDineroDes(ManejoArchivos.getDineroDes() - Constantes.LASAGNA_PRECIO);
            ManejoArchivos.setAlimentacionDes(ManejoArchivos.getAlimentacionDes() + Constantes.LASAGNA_PUNTOS);
            System.out.println("dinero" + ManejoArchivos.getDineroDes());
            textoDinero.setText(String.valueOf(ManejoArchivos.getDineroDes()) + " $$");
            UILivingRoom.pbAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);
            UILivingRoom.piAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);

        }
    }

    /**
     *Metodo para restar dinero y aumentar la alimentacion acorde al alimento "Pizza"
     */
    public void confiBtnPizza() {
        if (ManejoArchivos.getDineroDes() > 0) {
            ManejoArchivos.setDineroDes(ManejoArchivos.getDineroDes() - Constantes.PIZZA_PRECIO);
            ManejoArchivos.setAlimentacionDes(ManejoArchivos.getAlimentacionDes() + Constantes.PIZZA_PUNTOS);
            System.out.println("dinero" + ManejoArchivos.getDineroDes());
            textoDinero.setText(String.valueOf(ManejoArchivos.getDineroDes()) + " $$");
            UILivingRoom.pbAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);
            UILivingRoom.piAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);
        }
    }

    /**
     *Metodo para restar dinero y aumentar la alimentacion acorde al alimento "Pollo"
     */
    public void confiBtnPollo() {
        if (ManejoArchivos.getDineroDes() > 0) {
            ManejoArchivos.setDineroDes(ManejoArchivos.getDineroDes() - Constantes.POLLO_PRECIO);
            ManejoArchivos.setAlimentacionDes(ManejoArchivos.getAlimentacionDes() + Constantes.POLLO_PUNTOS);
            System.out.println("dinero" + ManejoArchivos.getDineroDes());
            textoDinero.setText(String.valueOf(ManejoArchivos.getDineroDes()) + " $$");
            UILivingRoom.pbAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);
            UILivingRoom.piAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);
        }
    }

    /**
     *Metodo que retorna el pane de Comedor
     * @return Pane principal de la clase Comedor.
     */
    public BorderPane getRoot() {
        return root;
    }

}
