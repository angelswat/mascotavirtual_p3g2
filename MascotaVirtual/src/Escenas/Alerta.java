/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Escenas;

import java.io.IOException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import mascotavirtual.MascotaVirtual;

/**
 *Clase para generar aviso cuando los datos ingresados son erroneos
 * @author Angel Antonio Encalada Davila
 */
public class Alerta {

    private BorderPane root;
    private Button btnAceptar;
    private Label lblMensaje;
    private String texto;
    private static Stage alertaStage;

    private HBox centro;
    private HBox inferior;

    /**
     *Constructor de la clase
     */
    public Alerta() {
        iniciarControles();
        manejarEvento();
    }

    /**
     *Metodo que se encarga de inicializar todos los elementos de la clase
     */
    public void iniciarControles() {
        root = new BorderPane();
        btnAceptar = new Button();
        lblMensaje = new Label();
        centro = new HBox();
        inferior = new HBox();
        texto = new String("Please, check the entered information !!!");

        Image image = new Image(getClass().getResource("/Imagen/UIinicio/aceptar.png").toExternalForm());
        ImageView iv1 = new ImageView();
        iv1.setFitHeight(75);
        iv1.setFitWidth(200);
        iv1.setImage(image);

        btnAceptar.setGraphic(iv1);
        inferior.getChildren().add(btnAceptar);
        inferior.setAlignment(Pos.CENTER);

        lblMensaje.setFont(Font.font(30));
        lblMensaje.setText(texto);
        centro.getChildren().add(lblMensaje);
        centro.setAlignment(Pos.CENTER);

        root.setCenter(centro);
        root.setBottom(inferior);
    }

    /**
     *Metodo para asiganar la accion al boton Aceptar
     */
    public void manejarEvento() {
        btnAceptar.setOnAction(e -> btnAceptar());
    }

    /**
     *Metodo para cerrar la pantalla cuando se de clic en Aceptar
     */
    public void btnAceptar() {
        alertaStage.close();
    }

    /**
     *Metodo que devuelve el Pane que contiene a todos los elementos de la clase
     * @return Pane con la clase principal de la clase Alerta
     */
    public BorderPane getRoot() {
        return root;
    }

    /**
     *Metodo que se encarga de lanzar el mensaje cuando los datos son incorrectos
     */
    public static void lanzarAlerta() {
        Alerta alerta = new Alerta();
        alertaStage = new Stage();
        MascotaVirtual mascota = new MascotaVirtual();
        try {
            mascota.start(alertaStage, alerta.getRoot());
        } catch (IOException ex1) {
            System.out.println("Excepcion al lanzar alerta");
            System.out.println(ex1);
        }
    }
}
