/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Escenas;

import Globales.Constantes;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import mascotavirtual.MascotaVirtual;

/**
 *Clase Principal en la cual el usuario procedera a registrarse
 * @author Usuario
 */
public class UIPrincipal {

    private Button btn1;
    private Button btn2;
    private Button btn3;
    private BorderPane root;
    private VBox centro;
    private HBox superior;
    private Label lbl;
    public static Stage entradaStage;
    public static Stage registroStage;

    /**
     *Constructor de la clase
     */
    public UIPrincipal() {
        iniciarPantalla();
        manejarEventos();
    }

    /**
     *Metodo que se encarga de cargar todos los elementos de la clase
     *
     */
    public void iniciarPantalla() {
        root = new BorderPane();
        centro = new VBox();
        btn1 = new Button();
        btn2 = new Button();
        btn3 = new Button();
        lbl = new Label();
        superior = new HBox();

        Image image = new Image(getClass().getResource("/Imagen/UIinicio/playbutton.png").toExternalForm());
        ImageView iv1 = new ImageView();
        iv1.setFitHeight(75);
        iv1.setFitWidth(175);
        iv1.setImage(image);

        Image image2 = new Image(getClass().getResource("/Imagen/UIinicio/signupbutton.png").toExternalForm());
        ImageView iv2 = new ImageView();
        iv2.setFitHeight(75);
        iv2.setFitWidth(175);
        iv2.setImage(image2);

        Image image3 = new Image(getClass().getResource("/Imagen/UIinicio/exitbutton.png").toExternalForm());
        ImageView iv3 = new ImageView();
        iv3.setFitHeight(75);
        iv3.setFitWidth(175);
        iv3.setImage(image3);

        Image image4 = new Image(getClass().getResource("/Imagen/UIinicio/petbutton.png").toExternalForm());
        ImageView iv4 = new ImageView();
        iv4.setImage(image4);

        Image image5 = new Image(getClass().getResource("/Imagen/UIinicio/background.png").toExternalForm());
        BackgroundImage fondo = new BackgroundImage(image5, null, null, null, new BackgroundSize(Constantes.ANCHOPANTALLA, Constantes.ALTURAPANTALLA, false, false, false, false));

        btn1.setGraphic(iv1);
        btn2.setGraphic(iv2);
        btn3.setGraphic(iv3);
        lbl.setGraphic(iv4);

        centro.getChildren().addAll(btn1, btn2, btn3);
        centro.setAlignment(Pos.CENTER);
        centro.setSpacing(10);
        superior.getChildren().addAll(lbl);
        superior.setAlignment(Pos.CENTER);
        superior.setSpacing(40);
        root.setTop(superior);
        root.setCenter(centro);
        root.setBackground(new Background(fondo));

    }

    /**
     *Metodo que retorna el Pane con todos sus elementos
     * @return Pane de la clase UIPrinciapal
     */
    public BorderPane getRoot() {
        return root;
    }

    /**
     *Metodo que asigna a los botones Jugar,Registrase y Salir sus respectivas
     * acciones
     */
    public void manejarEventos() {
        btn1.setOnAction(e -> {
            try {
                jugar();
            } catch (IOException ex) {
                Logger.getLogger(UIPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        btn2.setOnAction(e -> {
            try {
                registrarse();
            } catch (IOException ex) {
                Logger.getLogger(UIPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        btn3.setOnAction(e -> salir());
    }

    /**
     *Metodo que carga la escena para que el usuario juegue una partida ya creada
     * @throws IOException Lanza alerta de fallo de escena.
     */
    public static void jugar() throws IOException {
        UIEntrada entrada= new UIEntrada();
        MascotaVirtual mascota=new MascotaVirtual();
        entradaStage= new Stage();
        mascota.start(entradaStage,entrada.getRoot());
    }

    /**
     *Metodo que carga la escena para que el usuario se registre en el juego
     * @throws IOException Lanza alerta de fallo de escena.
     */
    public static void registrarse() throws IOException {
        UIregistro registro= new UIregistro();
        MascotaVirtual mascota= new MascotaVirtual();
        registroStage= new Stage();
        mascota.start(registroStage,registro.getRoot());
    }

    /**
     *Metodo para cerrar el juego
     */
    public static void salir() {
        System.exit(0);
    }

}
