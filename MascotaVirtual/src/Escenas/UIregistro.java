/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Escenas;

import Componentes.ManejoArchivos;
import Globales.Constantes;
import java.io.IOException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *Clase que se encarga del registro de un nuevo jugador
 * @author Usuario
 */
public class UIregistro {

    private BorderPane root;
    private Label lblTitulo;
    private Label lblNombre;
    private Label lblPetName;
    private Label lblEscoger;
    private TextField txtNombre;
    private TextField txtPetName;
    private Button btnSign;

    private Button btnMario;
    private Button btnDog;
    private Button btnTom;

    private HBox name;
    private HBox petname;
    private HBox pets;
    private HBox superior;
    private HBox inferior;
    private VBox contenido;

    /**
     *Constructor de la clase
     */
    public UIregistro() {
        iniciarControles();
    }

    /*
    *Metodo que se encarga de cargar todos los elemtentos de la escena
    */
    public void iniciarControles() {

        lblTitulo = new Label();
        lblNombre = new Label();
        lblPetName = new Label();
        lblEscoger = new Label();
        txtNombre = new TextField();
        txtPetName = new TextField();
        btnSign = new Button();
        name = new HBox();
        petname = new HBox();
        pets = new HBox();
        contenido = new VBox();
        inferior = new HBox();
        superior = new HBox();

        btnMario = new Button();
        btnTom = new Button();
        btnDog = new Button();

        root = new BorderPane();

        Image image1 = new Image(getClass().getResource("/Imagen/UIregistro/register.png").toExternalForm());
        ImageView imTitulo = new ImageView();
        imTitulo.setFitHeight(75);
        imTitulo.setFitWidth(200);
        imTitulo.setImage(image1);

        Image image2 = new Image(getClass().getResource("/Imagen/UIregistro/name.png").toExternalForm());
        ImageView imNombre = new ImageView();
        imNombre.setFitHeight(50);
        imNombre.setFitWidth(100);
        imNombre.setImage(image2);

        Image image3 = new Image(getClass().getResource("/Imagen/UIregistro/petname.png").toExternalForm());
        ImageView imPetName = new ImageView();
        imPetName.setFitHeight(50);
        imPetName.setFitWidth(100);
        imPetName.setImage(image3);

        Image image4 = new Image(getClass().getResource("/Imagen/UIregistro/choose.png").toExternalForm());
        ImageView imEscoger = new ImageView();
        imEscoger.setFitHeight(50);
        imEscoger.setFitWidth(100);
        imEscoger.setImage(image4);

        Image image6 = new Image(getClass().getResource("/Imagen/mascotas/tom.png").toExternalForm());
        ImageView imTom = new ImageView();
        imTom.setFitHeight(150);
        imTom.setFitWidth(75);
        imTom.setImage(image6);

        Image image7 = new Image(getClass().getResource("/Imagen/mascotas/dog.png").toExternalForm());
        ImageView imDog = new ImageView();
        imDog.setFitHeight(150);
        imDog.setFitWidth(75);
        imDog.setImage(image7);

        Image image8 = new Image(getClass().getResource("/Imagen/mascotas/Mario.png").toExternalForm());
        ImageView imMario = new ImageView();
        imMario.setFitHeight(150);
        imMario.setFitWidth(75);
        imMario.setImage(image8);

        Image image9 = new Image(getClass().getResource("/Imagen/UIregistro/done.png").toExternalForm());
        ImageView imDone = new ImageView();
        imDone.setFitHeight(50);
        imDone.setFitWidth(100);
        imDone.setImage(image9);

        Image image5 = new Image(getClass().getResource("/Imagen/UIregistro/background.png").toExternalForm());
        BackgroundImage fondo = new BackgroundImage(image5, null, null, null, new BackgroundSize(Constantes.ANCHOPANTALLA, Constantes.ALTURAPANTALLA, false, false, false, false));

        lblTitulo.setGraphic(imTitulo);
        lblTitulo.setAlignment(Pos.CENTER);
        lblNombre.setGraphic(imNombre);
        lblPetName.setGraphic(imPetName);
        lblEscoger.setGraphic(imEscoger);

        btnTom.setGraphic(imTom);
        btnMario.setGraphic(imMario);
        btnDog.setGraphic(imDog);
        btnSign.setGraphic(imDone);

        name.getChildren().addAll(lblNombre, txtNombre);
        name.setAlignment(Pos.CENTER);
        name.setSpacing(10);

        petname.getChildren().addAll(lblPetName, txtPetName);
        petname.setAlignment(Pos.CENTER);
        petname.setSpacing(10);

        pets.getChildren().addAll(btnTom, btnMario, btnDog);
        pets.setAlignment(Pos.CENTER);
        pets.setSpacing(20);

        contenido.getChildren().addAll(name, petname, lblEscoger, pets);
        contenido.setSpacing(10);
        contenido.setAlignment(Pos.CENTER);

        superior.getChildren().addAll(lblTitulo);
        superior.setAlignment(Pos.CENTER);

        inferior.getChildren().addAll(btnSign);
        inferior.setAlignment(Pos.CENTER);

        root.setCenter(contenido);
        root.setBottom(inferior);
        root.setTop(superior);
        root.setBackground(new Background(fondo));

        btnSign.setOnAction(e -> {
            try {
                btnRegistrar();
            } catch (IOException ex) {
                System.out.println("Serializar IOException" + ex);
            }
        });

        btnDog.setOnAction(e -> btnScooby());
        btnMario.setOnAction(e -> btnMario());
        btnTom.setOnAction(e -> btnTom());
    }

    /**
     *Metodo para obtener el nombre del usuario ingresado
     */
    public void obtenerTexto() {
        ManejoArchivos.setNombreUsuarioDes(txtNombre.getText());
    }

    /**
     *Metodo para obtener el nombre de la mascota ingresada
     */
    public void obtenerNombrePet() {
        ManejoArchivos.setNombreMascotaDes(txtPetName.getText());
    }

    /**
     *Metodo que devuelve el Pane con todos los elementos
     * @return
     */
    public BorderPane getRoot() {
        return root;
    }

    /**
     *Metodo que se encarga de guardar los datos ingresados del nuevo jugador
     * @throws IOException Lanza un error en case de que el archivo no exista
     */
    public void btnRegistrar() throws IOException {
        if (!txtNombre.getText().isEmpty() && !txtPetName.getText().isEmpty()) {
            obtenerNombrePet();
            obtenerTexto();
            ManejoArchivos.serializacion();
            System.out.println("Serializado");
            ManejoArchivos.setDireccionDes("");
            UIPrincipal.registroStage.close();
        } else {
            Alerta.lanzarAlerta();
        }
    }

    /**
     *Metodo que asigna la direccion de la imagen acorde a la mascota
     * seleccionada- "Scooby"
     */
    public void btnScooby() {
        ManejoArchivos.setDireccionDes("/Imagen/mascotas/dog.png");
    }

    /**
     *Metodo que asigna la direccion de la imagen acorde a la mascota
     * seleccionada- "Mario"
     */
    public void btnMario() {
        ManejoArchivos.setDireccionDes("/Imagen/mascotas/Mario.png");
    }

    /**
     *Metodo que asigna la direccion de la imagen acorde a la mascota
     * seleccionada- "Tom"
    */
    public void btnTom() {
        ManejoArchivos.setDireccionDes("/Imagen/mascotas/tom.png");
    }
}
