/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Escenas;

import Componentes.ManejoArchivos;
import Globales.Constantes;
import java.io.IOException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import mascotavirtual.MascotaVirtual;
import Escenas.UILivingRoom;
import javafx.event.EventHandler;
import javafx.stage.Popup;
import javafx.stage.WindowEvent;

/**
 *Clase que permite el acceso al juego
 * @author Angel Antonio Encalada Davila
 */
public class UIEntrada {

    private Label lblTitulo;
    private Label lblNombre;
    private TextField txtNombre;
    private Button btnLogin;
    private BorderPane root;
    private HBox superior;
    private HBox centro;
    private HBox inferior;
    public static Stage salaStage;

    
    /**
     *Constructor de la clase
     */
    UIEntrada() {
        iniciarControles();
        manejarEvento();
    }

    /**
     *Metodo para inicializar todos los elementos para cargar el juego
     */
    public void iniciarControles() {
        lblTitulo = new Label();
        lblNombre = new Label();
        txtNombre = new TextField();
        btnLogin = new Button();
        root = new BorderPane();
        superior = new HBox();
        centro = new HBox();
        inferior = new HBox();

        Image image1 = new Image(getClass().getResource("/Imagen/UIinicio/petbutton.png").toExternalForm());
        ImageView imTitulo = new ImageView();
        imTitulo.setImage(image1);

        Image image2 = new Image(getClass().getResource("/Imagen/UIregistro/username.png").toExternalForm());
        ImageView imName = new ImageView();
        imName.setFitHeight(100);
        imName.setFitWidth(200);
        imName.setImage(image2);

        Image image3 = new Image(getClass().getResource("/Imagen/UIregistro/login.png").toExternalForm());
        ImageView imLogin = new ImageView();
        imLogin.setFitHeight(75);
        imLogin.setFitWidth(150);
        imLogin.setImage(image3);

        Image image4 = new Image(getClass().getResource("/Imagen/UIregistro/fondo2.png").toExternalForm());
        BackgroundImage fondo = new BackgroundImage(image4, null, null, null, new BackgroundSize(Constantes.ANCHOPANTALLA, Constantes.ALTURAPANTALLA, false, false, false, false));

        lblTitulo.setGraphic(imTitulo);
        superior.getChildren().addAll(lblTitulo);
        superior.setAlignment(Pos.CENTER);
        superior.setSpacing(20);

        lblNombre.setGraphic(imName);
        txtNombre.setMinSize(200, 75);
        txtNombre.setFont(Font.font(20));
        centro.getChildren().addAll(lblNombre, txtNombre);
        centro.setAlignment(Pos.CENTER);
        centro.setSpacing(20);

        btnLogin.setGraphic(imLogin);
        inferior.getChildren().add(btnLogin);
        inferior.setAlignment(Pos.CENTER);
        inferior.setSpacing(20);

        root.setTop(superior);
        root.setCenter(centro);
        root.setBottom(inferior);
        root.setBackground(new Background(fondo));
    }

    /**
     *Metodo que retorna la escena
     * @return Pane de la clase UIEntrada
     */
    public BorderPane getRoot() {
        return root;
    }

    /**
     *Metodo con la configuracion del boton "Login", el cual carga la partida
     * guardada anteriormente con el nombre del usuario
     */
    public void btnLogin() {
        System.out.println("Ingreso a btnLogin()");
        Popup popUp = new Popup();
        popUp.setX(300);
        popUp.setY(200);
        Label lblPopUp = new Label("Partida Guardada");
        popUp.getContent().add(lblPopUp);
        ManejoArchivos.deserializacion(txtNombre.getText());
        System.out.println(ManejoArchivos.getDireccionDes());
        
        UILivingRoom sala = new UILivingRoom(ManejoArchivos.getDireccionDes());
        MascotaVirtual mascota = new MascotaVirtual();
        salaStage = new Stage();
        try {
            mascota.start(salaStage, sala.getRoot());
        } catch (IOException ex) {
            System.out.println("Error al crear escena de sala " + ex);
        }
        UIPrincipal.entradaStage.close();
        salaStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                System.out.println("cerrar");
                popUp.show(salaStage);
                try {
                    ManejoArchivos.serializacionFinal();
                } catch (IOException ex) {
                    System.out.println("error" + ex);
                }
            }
        });
    }

    /**
     *Metodo para asignar el evento al boton Login
     *
     */    
    public void manejarEvento() {
        System.out.println("Ingreso a manejar evento");
        btnLogin.setOnAction(e -> btnLogin());
    }
}
