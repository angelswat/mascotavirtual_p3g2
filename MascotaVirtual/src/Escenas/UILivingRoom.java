/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Escenas;

import Componentes.ManejoArchivos;
import Componentes.TimeLine;
import Globales.Constantes;
import Juegos.UIJuego;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.animation.Animation.Status.RUNNING;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;
import mascotavirtual.MascotaVirtual;

/**
 * Clase de la sala principal, donde permanece la mascota virtual.
 *
 * @author Ediso
 */
public final class UILivingRoom implements Serializable {

    private Label lblAnios;
    private Label lblTextAnios;
    private HBox anios;

    /**
     * Escena del juego de la mascota
     */
    public static Stage juegoStage;

    private Button btnComida;
    private Button btnJuego;
    private Button btnLimpieza;

    private Label lblDinero;

    /**
     * Muestra en tiempo real el dinero de la mascota
     */
    public static Label lblTextDinero;
    private HBox dinero;
    private Label animoActual;
    private Label alimentacionActual;
    private Label limpiezaActual;

    private BorderPane root;
    private VBox superior;
    private HBox superiorContenido;

    private Pane pane;
    private String direccion;

    /**
     * Muestra el animo de la mascota en una barra
     */
    public static ProgressBar pbAnimo;

    /**
     * Muestra la alimentacion de la mascota en una barra
     */
    public static ProgressBar pbAlimentacion;

    /**
     * Muestra la limpieza de la mascota en una barra
     */
    public static ProgressBar pbLimpieza;

    /**
     * Indicador circular de animo
     */
    public static ProgressIndicator piAnimo;

    /**
     * Indicador circular de alimentacion
     */
    public static ProgressIndicator piAlimentacion;

    /**
     * Indicador circular de limpieza
     */
    public static ProgressIndicator piLimpieza;

    private HBox animo;
    private HBox alimentacion;
    private HBox limpieza;
    private VBox indicadores;
    private VBox opciones;
    private HBox caja;
    private HBox informacionMascota;

    private Label lblAnimo;
    private Label lblAlimentacion;
    private Label lblLimpieza;
    private ImageView imagenBasura;
    private SequentialTransition sequentialTransition;
    private Timeline vida;
    private Timeline reloj;
    private Timeline tiempoDinero;

    /**
     * Constructor de la clase, es la sala principal dentro del juego.
     *
     * @param direccion Path de la imagen de la mascota
     */
    public UILivingRoom(String direccion) {
        this.direccion = direccion;
        inicializarPantalla();
    }

    /**
     * Metodo que carga la mayor parte de componentes que integra la sala de la
     * mascota.
     */
    public void inicializarPantalla() {

        ImageView imOld = new ImageView(new Image("/Imagen/comida/old.png"));
        imOld.setFitHeight(50);
        imOld.setFitWidth(70);
        ImageView imMoney = new ImageView(new Image("/Imagen/comida/money.png"));
        imMoney.setFitHeight(50);
        imMoney.setFitWidth(70);

        pbAnimo = new ProgressBar((float) ManejoArchivos.getAnimoDes() / 10);
        pbAlimentacion = new ProgressBar((float) ManejoArchivos.getAlimentacionDes() / 10);
        pbLimpieza = new ProgressBar((float) ManejoArchivos.getLimpiezaDes() / 10);
        piAnimo = new ProgressIndicator((float) ManejoArchivos.getAnimoDes() / 10);
        piAlimentacion = new ProgressIndicator((float) ManejoArchivos.getAlimentacionDes() / 10);
        piLimpieza = new ProgressIndicator((float) ManejoArchivos.getLimpiezaDes() / 10);

        ImageView imTextAnimo = new ImageView(new Image("/Imagen/UIhabitacion/Animo.png"));
        imTextAnimo.setFitHeight(40);
        imTextAnimo.setFitWidth(80);
        ImageView imTextAlimentacion = new ImageView(new Image("/Imagen/UIhabitacion/Feeding.png"));
        imTextAlimentacion.setFitHeight(40);
        imTextAlimentacion.setFitWidth(80);
        ImageView imTextLimpieza = new ImageView(new Image("/Imagen/UIhabitacion/limpio.png"));
        imTextLimpieza.setFitHeight(40);
        imTextLimpieza.setFitWidth(80);

        animo = new HBox();
        alimentacion = new HBox();
        limpieza = new HBox();
        indicadores = new VBox();
        caja = new HBox();
        informacionMascota = new HBox();
        lblAnimo = new Label();
        lblAnimo.setGraphic(imTextAnimo);
        lblAlimentacion = new Label();
        lblAlimentacion.setGraphic(imTextAlimentacion);
        lblLimpieza = new Label();
        lblLimpieza.setGraphic(imTextLimpieza);
        alimentacionActual = new Label(String.valueOf(ManejoArchivos.getAlimentacionDes()));
        alimentacionActual.setTextFill(Color.WHITE);
        limpiezaActual = new Label(String.valueOf(ManejoArchivos.getLimpiezaDes()));
        limpiezaActual.setTextFill(Color.WHITE);
        animoActual = new Label(String.valueOf(ManejoArchivos.getAnimoDes()));
        animoActual.setTextFill(Color.WHITE);

        animo.getChildren().addAll(lblAnimo, pbAnimo, piAnimo);
        animo.setSpacing(10);
        animo.setAlignment(Pos.CENTER_LEFT);
        alimentacion.getChildren().addAll(lblAlimentacion, pbAlimentacion, piAlimentacion);
        alimentacion.setSpacing(10);
        alimentacion.setAlignment(Pos.CENTER_LEFT);
        limpieza.getChildren().addAll(lblLimpieza, pbLimpieza, piLimpieza);
        limpieza.setSpacing(10);
        limpieza.setAlignment(Pos.CENTER_LEFT);
        indicadores.getChildren().addAll(alimentacion, animo, limpieza);
        indicadores.setSpacing(1);
        indicadores.setAlignment(Pos.TOP_CENTER);
        indicadores.setLayoutX(400);
        indicadores.setLayoutY(500);

        lblAnios = new Label();
        lblAnios.setGraphic(imOld);
        lblTextAnios = new Label(Integer.toString(ManejoArchivos.getVidaDes()) + " AÑOS");
        lblTextAnios.setFont(Font.font(15));
        lblTextAnios.setTextFill(Color.WHITE);
        lblTextAnios.setTextAlignment(TextAlignment.LEFT);
        anios = new HBox();
        anios.getChildren().addAll(lblAnios, lblTextAnios);
        anios.setSpacing(20);
        anios.setAlignment(Pos.CENTER_LEFT);

        lblDinero = new Label();
        lblDinero.setGraphic(imMoney);
        lblTextDinero = new Label(ManejoArchivos.getDineroDes() + " $$");
        lblTextDinero.setFont(Font.font(15));
        lblTextDinero.setTextFill(Color.WHITE);
        lblTextDinero.setTextAlignment(TextAlignment.LEFT);
        dinero = new HBox();
        dinero.getChildren().addAll(lblDinero, lblTextDinero);
        dinero.setSpacing(20);
        dinero.setAlignment(Pos.CENTER_LEFT);

        Image image = new Image(getClass().getResource("/Imagen/UIhabitacion/comida.png").toExternalForm());
        ImageView iv1 = new ImageView();
        iv1.setFitHeight(40);
        iv1.setFitWidth(40);
        iv1.setImage(image);

        Image image2 = new Image(getClass().getResource("/Imagen/UIhabitacion/limpieza.png").toExternalForm());
        ImageView iv2 = new ImageView();
        iv2.setFitHeight(40);
        iv2.setFitWidth(40);
        iv2.setImage(image2);

        Image image3 = new Image(getClass().getResource("/Imagen/UIhabitacion/jugar.png").toExternalForm());
        ImageView iv3 = new ImageView();
        iv3.setFitHeight(40);
        iv3.setFitWidth(40);
        iv3.setImage(image3);

        Image image4 = new Image(getClass().getResource("/Imagen/UIhabitacion/scene3.png").toExternalForm());
        BackgroundImage fondo = new BackgroundImage(image4, null, null, null, new BackgroundSize(Constantes.ANCHOPANTALLA, Constantes.ALTURAPANTALLA, false, false, false, false));

        btnComida = new Button();
        btnComida.setGraphic(iv1);
        btnLimpieza = new Button();
        btnLimpieza.setGraphic(iv2);
        btnJuego = new Button();
        btnJuego.setGraphic(iv3);

        superior = new VBox();
        opciones = new VBox();
        superiorContenido = new HBox();
        informacionMascota = new HBox();
        root = new BorderPane();

        superior.getChildren().addAll(anios, dinero);
        superior.setSpacing(5);
        superior.setAlignment(Pos.TOP_LEFT);

        caja.getChildren().addAll(superior, indicadores);
        caja.setAlignment(Pos.TOP_LEFT);
        caja.setSpacing(30);

        superiorContenido.getChildren().addAll(caja, opciones);
        superiorContenido.setAlignment(Pos.TOP_LEFT);
        superiorContenido.setSpacing(120);

        opciones.getChildren().addAll(btnComida, btnLimpieza, btnJuego);
        opciones.setAlignment(Pos.TOP_LEFT);
        opciones.setSpacing(10);

        informacionMascota.getChildren().addAll(alimentacionActual, animoActual, limpiezaActual);
        informacionMascota.setAlignment(Pos.TOP_LEFT);
        informacionMascota.setSpacing(10);

        root.setTop(superiorContenido);
        root.setBackground(new Background(fondo));
        root.setBottom(informacionMascota);

        btnComida.setOnAction(e -> comer());
        btnLimpieza.setOnAction(e -> limpiar());
        btnJuego.setOnAction(e -> jugar());

        pane = new Pane();
        pane.getChildren().addAll(ubicarImagen());
        root.setCenter(pane);

        Thread t = new Thread(new animacionBasura());
        t.start();
        crearBasura();

        Thread timeLine = new TimeLine();
        timeLine.start();

        manejarTiempo();

    }

    /**
     * Metodo que genera una imagen, mediante un path.
     *
     * @return Imagen de la mascota seleccionada por el usuario.
     */
    public ImageView ubicarImagen() {
        ImageView iv1 = new ImageView(new Image(direccion));
        iv1.setFitHeight(250);
        iv1.setFitWidth(200);
        animacionMascota(iv1);
        return iv1;
    }

    /**
     * Metodo que genera la escena de restaurante para la alimentacion de la
     * mascota.
     */
    public void comer() {
        Comedor comedor = new Comedor(ManejoArchivos.getDireccionDes());
        MascotaVirtual mascota = new MascotaVirtual();
        Stage primaryStage = new Stage();
        try {
            mascota.start(primaryStage, comedor.getRoot());
        } catch (IOException ex) {
            Logger.getLogger(UILivingRoom.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo que limpia el entorno de la sala, de la basura generada.
     */
    public void limpiar() {
        pane.getChildren().clear();
        pane.getChildren().addAll(ubicarImagen());
        ManejoArchivos.setLimpiezaDes(10);
        pbLimpieza.setProgress(((float) ManejoArchivos.getLimpiezaDes()) / 10);
        piLimpieza.setProgress(((float) ManejoArchivos.getLimpiezaDes()) / 10);
    }

    /**
     * Metodo que genera la escena del juego, donde la mascota eleva su animo y
     * gana dinero.
     */
    public void jugar() {
        UIJuego juego = new UIJuego();
        MascotaVirtual mascota = new MascotaVirtual();
        juegoStage = new Stage();
        try {
            mascota.start(juegoStage, juego);
        } catch (IOException ex) {
            Logger.getLogger(UILivingRoom.class.getName()).log(Level.SEVERE, null, ex);
        }
        ManejoArchivos.setAnimoDes(ManejoArchivos.getAnimoDes() + 2);
    }

    /**
     * Metodo que devuelve el root de la escena.
     *
     * @return Pane principal de la clase LivingRoom.
     */
    public BorderPane getRoot() {
        return root;
    }

    /**
     * Metodo que da animacion a la mascota dentro de la sala.
     *
     * @param m Nodo para el cual se realiza la animacion.
     */
    public void animacionMascota(Node m) {

        m.setLayoutX(50);
        m.setLayoutY(25);

        TranslateTransition translateTransition
                = new TranslateTransition(Duration.millis(5000), m);
        translateTransition.setFromX(100);
        translateTransition.setToX(200);
        translateTransition.setCycleCount(1);
        translateTransition.setAutoReverse(true);

        TranslateTransition translateTransition2
                = new TranslateTransition(Duration.millis(5000), m);
        translateTransition2.setFromX(300);
        translateTransition2.setToX(200);
        translateTransition2.setCycleCount(1);
        translateTransition2.setAutoReverse(true);

        FadeTransition fadeTransition
                = new FadeTransition(Duration.millis(1000), m);
        fadeTransition.setFromValue(1.0f);
        fadeTransition.setToValue(0.3f);
        fadeTransition.setCycleCount(1);
        fadeTransition.setAutoReverse(true);
        System.out.println(m.getLayoutX());
        System.out.println(m.getLayoutY());

        RotateTransition rotateTransition
                = new RotateTransition(Duration.millis(4000), m);
        rotateTransition.setByAngle(30f);
        rotateTransition.setCycleCount(2);
        rotateTransition.setAutoReverse(true);

        ScaleTransition scaleTransition
                = new ScaleTransition(Duration.millis(2000), m);
        scaleTransition.setFromX(1);
        scaleTransition.setFromY(1);
        scaleTransition.setToX(1.2);
        scaleTransition.setToY(1.2);

        scaleTransition.setAutoReverse(true);

        TranslateTransition translateTransition1
                = new TranslateTransition(Duration.millis(5000), m);
        translateTransition1.setFromY(40);
        translateTransition1.setToY(30);
        translateTransition1.setCycleCount(1);
        translateTransition1.setAutoReverse(true);

        TranslateTransition translateTransition5
                = new TranslateTransition(Duration.millis(5000), m);
        translateTransition5.setFromY(40);
        translateTransition5.setToY(30);
        translateTransition5.setCycleCount(1);
        translateTransition5.setAutoReverse(true);

        sequentialTransition = new SequentialTransition();
        sequentialTransition.getChildren().addAll(translateTransition, scaleTransition,
                rotateTransition, translateTransition1, translateTransition2, translateTransition5);
        sequentialTransition.setCycleCount(Timeline.INDEFINITE);
        sequentialTransition.setAutoReverse(true);

        sequentialTransition.play();

    }

    /**
     * Metodo que genera basura o suciedad aleatoriamente en la sala de la
     * mascota.
     */
    public void crearBasura() {
        imagenBasura = new ImageView(new Image("/imagen/UIhabitacion/basura.png"));
        imagenBasura.setFitHeight(50);
        imagenBasura.setFitWidth(50);
        imagenBasura.setY(240);
        int posX = (int) (Math.random() * 500) + 1;
        imagenBasura.setX(posX);
        pane.getChildren().addAll(imagenBasura);
    }

    /**
     * Inner class para la thread de generacion de basura.
     */
    public class animacionBasura implements Runnable {

        /**
         * Metodo sobreescrito RUN.
         */
        @Override
        public void run() {
            boolean condicion = true;
            while (condicion) {
                try {
                    Platform.runLater(() -> crearBasura());

                    int minutos = (int) (Math.random() * 5) + 1;
                    Thread.sleep(minutos * 60000);
                } catch (InterruptedException ex) {
                    System.out.println(ex);
                }
            }
        }
    }

    /**
     * Metodo que controla el paso del tiempo y las actividades secuenciales que
     * se producen.
     */
    public void manejarTiempo() {
        reloj = new Timeline();
        vida = new Timeline();
        tiempoDinero = new Timeline();

        KeyFrame kf1 = new KeyFrame(Duration.minutes(1), e -> controlParametros());
        KeyFrame kf2 = new KeyFrame(Duration.hours(1), e -> controlVida());
        KeyFrame kf3 = new KeyFrame(Duration.seconds(1), e -> actualizador());

        reloj.getKeyFrames().addAll(kf1);
        vida.getKeyFrames().addAll(kf2);
        reloj.setCycleCount(Timeline.INDEFINITE);
        vida.setCycleCount(Timeline.INDEFINITE);
        tiempoDinero.getKeyFrames().add(kf3);
        tiempoDinero.setCycleCount(Timeline.INDEFINITE);

        reloj.play();
        vida.play();
        tiempoDinero.play();

    }

    /**
     * Metodo para actualiza todos los labels que informan los estados de la
     * mascota.
     */
    public void actualizador() {
        lblTextDinero.setText(String.valueOf(ManejoArchivos.getDineroDes()) + " $$");
        pbAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);
        piAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);
        Comedor.pbAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);
        Comedor.piAlimentacion.setProgress(((float) ManejoArchivos.getAlimentacionDes()) / 10);
        pbAnimo.setProgress(((float) ManejoArchivos.getAnimoDes()) / 10);
        piAnimo.setProgress(((float) ManejoArchivos.getAnimoDes()) / 10);
        pbLimpieza.setProgress(((float) ManejoArchivos.getLimpiezaDes()) / 10);
        piLimpieza.setProgress(((float) ManejoArchivos.getLimpiezaDes()) / 10);

    }

    /**
     * Metodo que disminuye los estados de la mascota dependiendo de como
     * transcurre el tiempo.
     */
    public void restarVida() {
        if (ManejoArchivos.getVidaDes() > 0) {
            if (ManejoArchivos.getAlimentacionDes() < 1 || ManejoArchivos.getLimpiezaDes() < 1 || ManejoArchivos.getAnimoDes() < 1) {
                if (ManejoArchivos.getVidaDes() > 100) {
                    ManejoArchivos.setVidaDes(ManejoArchivos.getVidaDes() - 100);
                } else {
                    ManejoArchivos.setVidaDes(0);
                }

            }

            if (ManejoArchivos.getAlimentacionDes() < 1 && ManejoArchivos.getLimpiezaDes() < 1 && ManejoArchivos.getAnimoDes() < 1) {
                if (ManejoArchivos.getVidaDes() > 300) {
                    ManejoArchivos.setVidaDes(ManejoArchivos.getVidaDes() - 300);
                } else {
                    ManejoArchivos.setVidaDes(0);
                }

            }
        }
    }

    /**
     * Metodo para controlar los parametros de la mascota en tiempo real.
     */
    public void controlParametros() {
        if (vida.getStatus().equals(RUNNING)) {

            ManejoArchivos.setAnimoDes(ManejoArchivos.getAnimoDes() - 1);
            if (ManejoArchivos.getAnimoDes() < 0) {
                ManejoArchivos.setAnimoDes(0);

            }
            ManejoArchivos.setAlimentacionDes(ManejoArchivos.getAlimentacionDes() - 1);
            if (ManejoArchivos.getAlimentacionDes() < 0) {
                ManejoArchivos.setAlimentacionDes(0);

            }
            ManejoArchivos.setLimpiezaDes(ManejoArchivos.getLimpiezaDes() - 1);
            if (ManejoArchivos.getLimpiezaDes() < 0) {
                ManejoArchivos.setLimpiezaDes(0);

            }

            System.out.println("Ahora mi alimentacion es: " + ManejoArchivos.getAlimentacionDes());
            System.out.println("Ahora mi animo es: " + ManejoArchivos.getAnimoDes());
            System.out.println("Ahora mi limpieza es: " + ManejoArchivos.getLimpiezaDes());

            animoActual.setText(String.valueOf(ManejoArchivos.getAnimoDes()));
            alimentacionActual.setText(String.valueOf(ManejoArchivos.getAlimentacionDes()));
            limpiezaActual.setText(String.valueOf(ManejoArchivos.getLimpiezaDes()));
        }
    }

    /**
     * Metodo que verifica que el juego se lleve acorde a las reglas, en cuanto a la vida de la mascota.
     */
    public void controlVida() {
        if (ManejoArchivos.getVidaDes() == 0 || ManejoArchivos.getVidaDes() == 500) {

            System.out.println("Mi edad actual es: " + ManejoArchivos.getVidaDes());
            lblTextAnios.setText(String.valueOf(ManejoArchivos.getVidaDes()) + " AÑOS");
            System.out.println("Estoy muerto");

            vida.stop();
            reloj.stop();
            sequentialTransition.stop();
            System.out.println(String.valueOf(vida.getStatus()));

            try {
                ManejoArchivos.borrarJson(ManejoArchivos.getNombreUsuarioDes() + ".json");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(UILivingRoom.class.getName()).log(Level.SEVERE, null, ex);
            }
            UIEntrada.salaStage.close();
            UIPrincipal.entradaStage.close();
            System.exit(0);
        }

        if (vida.getStatus().equals(RUNNING)) {

            ManejoArchivos.setVidaDes(ManejoArchivos.getVidaDes() + 1);
            lblTextAnios.setText(String.valueOf(ManejoArchivos.getVidaDes()) + " AÑOS");
            System.out.println("Mi edad actual es: " + ManejoArchivos.getVidaDes());

            restarVida();
        }
    }
}
